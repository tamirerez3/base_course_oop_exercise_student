package Missions;

import AerialVehicles.AerialVehicle;
import Entities.AerialCapability;
import Entities.AttackingCapability;
import Entities.Coordinates;
import Entities.IntelligenceCapability;

public class IntelligenceMission extends Mission {
    private String region;

    public IntelligenceMission(Coordinates destination, String pilotName, AerialVehicle executingVehicle, String region) {
        super(destination, pilotName, executingVehicle);
        this.region = region;

    }

    @Override
    public String executeMission() throws AerialVehicleNotCompatibleException {
        AerialCapability[] capabilities = executingVehicle.getCapabilities();
        IntelligenceCapability cap = null;
        for (AerialCapability c : capabilities) {
            if (c instanceof IntelligenceCapability)
                cap = (IntelligenceCapability) c;
        }
        if (cap == null)
            throw new AerialVehicleNotCompatibleException(executingVehicle + "Missing Attacking capability");

        return pilotName + ": " + executingVehicle + " Collecting Data in " + region + " with: sensor type:" + cap.getSensorType();
    }
}
