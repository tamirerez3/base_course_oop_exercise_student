package Missions;

import AerialVehicles.AerialVehicle;
import Entities.Coordinates;

public abstract class Mission {
    private Coordinates destination;
    protected String pilotName;
    protected AerialVehicle executingVehicle;

    public Mission(Coordinates destination, String pilotName, AerialVehicle executingVehicle) {
        this.executingVehicle = executingVehicle;
        this.destination = destination;
        this.pilotName = pilotName;
    }

    public void begin() {
        System.out.println("Beginning Mission!");
        executingVehicle.flyTo(destination);
    }

    public void cancel() {
        System.out.println("Abort Mission!");
        executingVehicle.land(executingVehicle.getBaseLoc());
    }

    public void finish() {
        try {
            System.out.println(executeMission());
        } catch (AerialVehicleNotCompatibleException e) {
            System.out.println("Could not finish mission");
        }
        executingVehicle.land(executingVehicle.getBaseLoc());
        System.out.println("Finish Mission");
    }

    public abstract String executeMission() throws AerialVehicleNotCompatibleException;
}
