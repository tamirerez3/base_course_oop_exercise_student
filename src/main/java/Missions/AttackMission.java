package Missions;

import AerialVehicles.AerialVehicle;
import Entities.AerialCapability;
import Entities.AttackingCapability;
import Entities.Coordinates;

public class AttackMission extends Mission {
    String target;

    public AttackMission(Coordinates destination, String pilotName, AerialVehicle executingVehicle, String target) {
        super(destination, pilotName, executingVehicle);
        this.target = target;
    }

    @Override
    public String executeMission() throws AerialVehicleNotCompatibleException {
        AerialCapability[] capabilities = executingVehicle.getCapabilities();
        AttackingCapability cap = null;
        for (AerialCapability c : capabilities){
            if(c instanceof AttackingCapability)
                cap = (AttackingCapability) c;
        }
        if(cap == null)
            throw new AerialVehicleNotCompatibleException(executingVehicle + "Missing Attacking capability");

        return pilotName + ": " + executingVehicle + " Attacking " + target + " with: " + cap.getMissileType() + "X" + cap.getNumOfMissiles();
    }
}
