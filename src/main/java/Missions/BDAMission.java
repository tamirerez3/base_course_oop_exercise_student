package Missions;

import AerialVehicles.AerialVehicle;
import Entities.AerialCapability;
import Entities.BDACapability;
import Entities.Coordinates;

public class BDAMission extends Mission {
    private String objective;

    public BDAMission(Coordinates destination, String pilotName, AerialVehicle executingVehicle,String objective) {
        super(destination, pilotName, executingVehicle);
        this.objective = objective;
    }

    @Override
    public String executeMission() throws AerialVehicleNotCompatibleException {
        AerialCapability[] capabilities = executingVehicle.getCapabilities();
        BDACapability cap = null;
        for (AerialCapability c : capabilities) {
            if (c instanceof BDACapability)
                cap = (BDACapability) c;
        }
        if (cap == null)
            throw new AerialVehicleNotCompatibleException(executingVehicle + "Missing BDA capability");

        return pilotName + ": " + executingVehicle + " Taking pictures of " + objective + " with: " + cap.getCameraType();
    }
}
