package Entities;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class AttackingCapability implements AerialCapability {
    private int numOfMissiles;
    private MissileType missileType;
    public enum MissileType {Python, Amram, Spice250}

    public AttackingCapability(int numOfMissiles, MissileType type){
        this.missileType = type;
        this.numOfMissiles = numOfMissiles;
    }

    public int getNumOfMissiles(){
        return numOfMissiles;
    }
    public MissileType getMissileType(){
        return missileType;
    }
}
