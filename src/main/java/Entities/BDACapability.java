package Entities;

public class BDACapability implements AerialCapability {
    public enum CameraType {Regular, Thermal, NightVision}
    private CameraType cameraType;

    public BDACapability( CameraType cameraType){
        this.cameraType  = cameraType;
    }

    public CameraType getCameraType() {
        return cameraType;
    }
}
