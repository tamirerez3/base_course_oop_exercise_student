package Entities;

public class IntelligenceCapability implements AerialCapability{
    public enum SensorType {InfraRed, Elint}
    private SensorType sensorType;

    public IntelligenceCapability(SensorType sensorType){
        this.sensorType = sensorType;
    }

    public SensorType getSensorType() {
        return sensorType;
    }

}
