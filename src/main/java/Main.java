import AerialVehicles.AttackingVehicles.F16;
import AerialVehicles.Drones.HermesDrones.Zik;
import Entities.AttackingCapability;
import Entities.BDACapability;
import Entities.Coordinates;
import Entities.IntelligenceCapability;
import Missions.AttackMission;
import Missions.BDAMission;
import Missions.IntelligenceMission;

public class Main {

    public static void main(String[] args) {
        runIntelligenceMission();
        runAttackingMission();
        runBDAMission();
    }

    private static void runBDAMission() {
        BDACapability bda = new BDACapability(BDACapability.CameraType.Regular);
        F16 f16 = new F16(new Coordinates(1.0,1.0),bda);
        BDAMission bdaMission = new BDAMission(new Coordinates(2.0,2.0),"Gavriel",f16,"bdaing");
        bdaMission.begin();
        bdaMission.finish();
    }

    private static void runAttackingMission() {
        AttackingCapability attack = new AttackingCapability(4,AttackingCapability.MissileType.Python);
        F16 f16 = new F16(new Coordinates(1.0,1.0),attack);
        AttackMission attackMission = new AttackMission(new Coordinates(2.0,2.0),"Gavriel",f16,"mordor");
        attackMission.begin();
        attackMission.finish();
    }

    private static void runIntelligenceMission() {
        IntelligenceCapability intel = new IntelligenceCapability(IntelligenceCapability.SensorType.Elint);
        Zik zik = new Zik(new Coordinates(1.0,1.0),intel);
        IntelligenceMission intelligenceMission = new IntelligenceMission(new Coordinates(2.0,2.0),"Gavriel",zik,"mordor");
        intelligenceMission.begin();
        intelligenceMission.finish();
    }
}
