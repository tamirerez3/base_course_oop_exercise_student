package AerialVehicles.AttackingVehicles;


import AerialVehicles.AttackingVehicles.AttackingAircraft;
import Entities.*;

public class F15 extends AttackingAircraft {

    public F15(Coordinates baseLoc, AerialCapability... capabilities) {
        super(baseLoc, capabilities);
    }

    @Override
    public String toString() {
        return "F15";
    }


}
