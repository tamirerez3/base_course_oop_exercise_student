package AerialVehicles.AttackingVehicles;


import AerialVehicles.AttackingVehicles.AttackingAircraft;
import Entities.*;

public class F16 extends AttackingAircraft {

    public F16(Coordinates baseLoc, AerialCapability... capabilities) {
        super(baseLoc, capabilities);
    }

    @Override
    public String toString() {
        return "F16";
    }
}
