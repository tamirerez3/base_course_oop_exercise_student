package AerialVehicles.AttackingVehicles;

import AerialVehicles.AerialVehicle;
import Entities.AerialCapability;
import Entities.Coordinates;

public abstract class AttackingAircraft extends AerialVehicle {
    private final int maxFlightTime = 250;

    public AttackingAircraft(Coordinates baseLoc, AerialCapability... capabilities) {
        super(baseLoc, capabilities);
    }

    @Override
    public void check() {
        if(flightTimeSinceLastRepair >= maxFlightTime){
            status = flightStatus.NotReady;
            repair();
        }
        else{
            status = flightStatus.Ready;
        }
    }
}
