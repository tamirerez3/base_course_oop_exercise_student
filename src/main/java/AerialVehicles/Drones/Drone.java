package AerialVehicles.Drones;

import AerialVehicles.AerialVehicle;
import Entities.AerialCapability;
import Entities.Coordinates;

public abstract class Drone extends AerialVehicle {

    public Drone(Coordinates baseLoc, AerialCapability... capabilities) {
        super(baseLoc, capabilities);
    }

    public String hoverOverLocation(Coordinates destination){
        status = flightStatus.Midflight;
        String toPrint = "Hovering Over: " + destination.getLatitude() + ", " + destination.getLongitude();
        System.out.println(toPrint);
        return toPrint;
    }
}
