package AerialVehicles.Drones.HermesDrones;

import AerialVehicles.Drones.Drone;
import Entities.AerialCapability;
import Entities.Coordinates;

public abstract class Hermes extends Drone {

    private final int maxFlightTime = 100;

    public Hermes(Coordinates baseLoc, AerialCapability... capabilities) {
        super(baseLoc, capabilities);
    }

    @Override
    public void check() {
        if(flightTimeSinceLastRepair >= maxFlightTime){
            status = flightStatus.NotReady;
            repair();
        }
        else{
            status = flightStatus.Ready;
        }
    }
}
