package AerialVehicles.Drones.HermesDrones;


import Entities.*;

public class Zik extends Hermes{
    public Zik(Coordinates baseLoc, AerialCapability... capabilities) {
        super(baseLoc, capabilities);
    }

    @Override
    public String toString() {
        return "Zik";
    }
}