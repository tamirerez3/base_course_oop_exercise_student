package AerialVehicles.Drones.HermesDrones;

import Entities.*;

public class Kochav extends Hermes {

    public Kochav(Coordinates baseLoc, AerialCapability... capabilities) {
        super(baseLoc, capabilities);
    }

    @Override
    public String toString() {
        return "Kochav";
    }
}
