package AerialVehicles.Drones.HaronDrones;

import Entities.*;

public class Eitan extends Haron {
    public Eitan(Coordinates baseLoc, AerialCapability... capabilities) {
        super(baseLoc, capabilities);
    }

    @Override
    public String toString() {
        return "Eitan";
    }
}
