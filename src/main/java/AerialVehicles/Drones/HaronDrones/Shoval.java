package AerialVehicles.Drones.HaronDrones;


import Entities.*;

public class Shoval extends Haron {


    public Shoval(Coordinates baseLoc, AerialCapability... capabilities) {
        super(baseLoc, capabilities);
    }

    @Override
    public String toString() {
        return "Shoval";
    }
}

