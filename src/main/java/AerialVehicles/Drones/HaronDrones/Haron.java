package AerialVehicles.Drones.HaronDrones;

import AerialVehicles.Drones.Drone;
import Entities.AerialCapability;
import Entities.Coordinates;

public abstract class Haron extends Drone {

    private final int maxFlightTime = 150;

    public Haron(Coordinates baseLoc, AerialCapability... capabilities) {
        super(baseLoc, capabilities);
    }

    @Override
    public void check() {
        if(flightTimeSinceLastRepair >= maxFlightTime){
            status = flightStatus.NotReady;
            repair();
        }
        else{
            status = flightStatus.Ready;
        }
    }

}
