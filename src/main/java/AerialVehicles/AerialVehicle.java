package AerialVehicles;


import Entities.AerialCapability;
import Entities.Coordinates;
import com.sun.source.tree.BreakTree;

import java.util.Arrays;

public abstract class AerialVehicle {

    public enum flightStatus {Ready, NotReady, Midflight}
    protected int flightTimeSinceLastRepair;
    private AerialCapability[] capabilities;
    private Coordinates baseLoc;
    protected flightStatus status;

    public AerialVehicle(Coordinates baseLoc, AerialCapability... capabilities) {
        this.capabilities = capabilities;
        flightTimeSinceLastRepair = 0;
        status = flightStatus.Ready;
        this.baseLoc = baseLoc;
    }

    public void flyTo(Coordinates destination) {
        if (status.equals(flightStatus.Ready)) {
            System.out.println("Flying to: " + destination);
            status = flightStatus.Midflight;
        } else if (status.equals(flightStatus.NotReady)) {
            System.out.println("Aerial Vehicle isn't ready to fly");
        } else if (status.equals(flightStatus.Midflight)) {
            System.out.println("Aerial Vehicle is already in flight");
        }
    }

    public void land(Coordinates destination) {
        System.out.println("Landing on: " + destination);
        check();
    }

    public abstract void check();

    public void repair(){
        flightTimeSinceLastRepair = 0;
        status = flightStatus.Ready;
    }

    public AerialCapability[] getCapabilities(){
        return Arrays.copyOf(capabilities,capabilities.length);
    }
    public Coordinates getBaseLoc(){
        return baseLoc;
    }

    public abstract String toString();
}
